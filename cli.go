// Package cli provides convenient means of parsing command line args.
package cli

import (
	"fmt"
	"math"
	"os"
)

type flag struct {
	name     string
	symbol   uint8
	required bool
	contexts uint64
	parsed   *bool
	paramc   int
	params   []*string
}

// PrintUsage is a function that is called in case invalid args were provided.
var PrintUsage = printUsage

// FlagsRequired defines wether atleast one arg (or flag) must be provided.
var FlagsRequired bool

var flagsShort map[string]*flag = make(map[string]*flag)
var flagsLong map[string]*flag = make(map[string]*flag)
var allFlags []*flag = make([]*flag, 0)
var valueFlag *flag
var contexts uint64

// RegisterFlag registers a new flag to be parsed. The required
// syntax for a flag can be defined with name and symbol (which is
// --name and -symbol respectively). Wether a flag is optional or
// not can be defined with required. Note that a symbol of 0 denotes
// a flag as value flag meaning that it may accept any literal and
// is treated as its own parameter in addition of beeing a flag
// (therefore paramc should always be atleast 1). Value flags may
// recieve other parameters aswell. Any command may contain only 1
// value flag at a time. It is possible to assign flags to one or
// more contexts. This is done by providing a bitmask where a set
// bit represents the membership for a context. When parsed any
// overlapping contexts for different flags will trigger a panic.
// The parsed pointer is used to store the result of the existence
// of a flag. Flags can recieve parameters themselfs. The required
// amount of parameters is defined with paramc. This is followed by
// a sequence of pointers, params, which are used to store the read
// parameters. If more pointers are provided than the value of
// paramc defines any remaining args will be treated as parameters
// and assigned to those pointers (therefore beeing optional
// parameters). The parsing of optional parameters stops once either
// of the following happens: There are no more arguments to parse,
// there are no more pointers to store results or an argument is
// equal to any of the registered flags that was not parsed yet and
// whose contexts were not used at that point.
func RegisterFlag(name string, symbol uint8, required bool, contexts uint64, parsed *bool, paramc int, params ...*string) {
	f := flag{name, symbol, required, contexts, parsed, paramc, params}

	if symbol == 0 {
		if valueFlag != nil {
			panic("value flag was already registered")
		}

		valueFlag = &f
	} else {
		short := "-" + string(symbol)

		if _, contains := flagsShort[short]; contains {
			panic("symbol was already registered for another flag")
		}

		long := "--" + name

		if _, contains := flagsLong[long]; contains {
			panic("name was already registered for another flag")
		}

		flagsShort[short] = &f
		flagsLong[long] = &f
	}

	allFlags = append(allFlags, &f)
}

// ParseArgs parses registered flags by accessing the command line
// args with os.Args[1:]. Results are stored in the storage pointed
// to by the pointers passed to RegisterFlag calls. In case a panic
// occurs an error message is printed followed by a call to
// PrintUsage. A call to this function will always dispose of
// registered flags afterwards.
func ParseArgs() {
	args := os.Args[1:]

	defer func() {
		if r := recover(); r != nil {
			defer panic(nil)
			os.Stderr.WriteString(fmt.Sprintf("Error: %s\n", r))
			PrintUsage()
		}

		reset()
	}()

	if FlagsRequired && len(args) == 0 {
		panic("no flags provided")
	}

	for i := 0; i < len(args); i++ {
		a := args[i]

		if f, contains := getFlag(a); contains {
			if *f.parsed {
				panic(fmt.Sprintf("flag '%s' was already provided", f.name))
			}

			if contexts&f.contexts != 0 {
				panic(fmt.Sprintf("flag '%s' is not allowed in this context", f.name))
			}

			contexts |= f.contexts
			*f.parsed = true
			i += parseValues(args[i+1:], f)
		} else if valueFlag != nil && !*valueFlag.parsed {
			if contexts&valueFlag.contexts != 0 {
				panic(fmt.Sprintf("value flag '<%s>' is not allowed in this context", valueFlag.name))
			}

			contexts |= valueFlag.contexts
			*valueFlag.parsed = true
			i += parseValues(args[i:], valueFlag) - 1
		} else {
			panic(fmt.Sprintf("unknown flag '%s'", a))
		}
	}

	if valueFlag != nil && valueFlag.required && (valueFlag.contexts&contexts) != valueFlag.contexts {
		panic(fmt.Sprintf("missing value flag '<%s>'", string(valueFlag.name)))
	}

	for _, f := range flagsShort {
		if f.required && (f.contexts&contexts) != f.contexts {
			panic(fmt.Sprintf("missing '%s' flag", string(f.name)))
		}
	}
}

func parseValues(args []string, f *flag) int {
	l := len(args)

	if l < f.paramc {
		panic(fmt.Sprintf("missing paramter for flag '%s'", f.name))
	}

	l = int(math.Min(float64(len(f.params)), float64(l)))

	for i := 0; i < l; i++ {
		a := args[i]

		if i >= f.paramc {
			if f, contains := getFlag(a); contains && !*f.parsed && f.contexts&contexts == 0 {
				l -= i
				break
			}
		}

		*f.params[i] = a
	}

	return l
}

func getFlag(key string) (*flag, bool) {
	if f, contains := flagsShort[key]; contains {
		return f, true
	}

	f, contains := flagsLong[key]
	return f, contains
}

func reset() {
	flagsShort = make(map[string]*flag)
	flagsLong = make(map[string]*flag)
	allFlags = make([]*flag, 0)
	valueFlag = nil
	contexts = 0
}

func printUsage() {
	// todo generate usage string from allFlags
}
