# CLI

Package cli provides convenient means of parsing command line args.

## Variables

FlagsRequired defines wether atleast one arg (or flag) must be provided.

```golang
var FlagsRequired bool
```

PrintUsage is a function that is called in case invalid args were provided.

```golang
var PrintUsage = func() {}
```

## Functions

### func [ParseArgs](/cli.go#L72)

`func ParseArgs()`

ParseArgs parses registered flags by accessing the command line
args with os.Args[1:]. Results are stored in the storage pointed
to by the pointers passed to RegisterFlag calls. In case a panic
occurs an error message is printed followed by a call to
PrintUsage. A call to this function will always dispose of
registered flags afterwards.

### func [RegisterFlag](/cli.go#L55)

`func RegisterFlag(name string, symbol uint8, required bool, contexts uint64, parsed *bool, paramc int, params ...*string)`

RegisterFlag registers a new flag to be parsed. The required
syntax for a flag can be defined with name and symbol (which is
--name and -symbol respectively). Wether a flag is optional or
not can be defined with required. Note that a symbol of 0 denotes
a flag as value flag meaning that it may accept any literal and
is treated as its own parameter in addition of beeing a flag
(therefore paramc should always be atleast 1). Value flags may
recieve other parameters aswell. Any command may contain only 1
value flag at a time. It is possible to assign flags to one or
more contexts. This is done by providing a bitmask where a set
bit represents the membership for a context. When parsed any
overlapping contexts for different flags will trigger a panic.
The parsed pointer is used to store the result of the existence
of a flag. Flags can recieve parameters themselfs. The required
amount of parameters is defined with paramc. This is followed by
a sequence of pointers, params, which are used to store the read
parameters. If more pointers are provided than the value of
paramc defines any remaining args will be treated as parameters
and assigned to those pointers (therefore beeing optional
parameters). The parsing of optional parameters stops once either
of the following happens: There are no more arguments to parse,
there are no more pointers to store results or an argument is
equal to any of the registered flags that was not parsed yet and
whose contexts were not used at that point.

---
Readme created from Go doc with [goreadme](https://github.com/posener/goreadme)
